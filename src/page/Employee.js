import React from 'react';

import { Page, Panel, Button, eventBus, Alert, Breadcrumbs, Input } from 'react-blur-admin';
import { Link } from 'react-router';
import { Row, Col } from 'react-flex-proto';
import '../css/Base.css';
import { addEmployeeAPI } from "../service/DataService";
export class Employee extends React.Component {

    constructor() {
        super();
        this.state = {
            employeeForm: {

                user_email: "",
                company_email: "",
                company_name: "",
                company_description: "",
                company_address: "",
                company_pin_code: "",
                company_website: "",
                company_contact: "",
                company_poc: "",
                company_poc_contact: "",
                user_password: ""
            },
            errors: {},
        }
    }

    addEmployee = (data) => {

        addEmployeeAPI(data).then(response => {
            if (response) {
                this.setState({
                    employeeForm: {

                        user_email: "",
                        company_email: "",
                        company_name: "",
                        company_description: "",
                        company_address: "",
                        company_pin_code: "",
                        company_website: "",
                        company_contact: "",
                        company_poc: "",
                        company_poc_contact: "",
                        user_password: ""
                    }
                });

            }

        })
    };



    handleChange = (key, event) => {


        let value = event.target.value;
        let employeeDetails = { ...this.state.employeeForm }
        employeeDetails[key] = value;
        this.setState({
            employeeForm: employeeDetails,
        })
    };

    handleSubmit = (event) => {
        event.preventDefault();

        if (this.validate()) {

            let employeeForm = {};

            employeeForm["user_email"] = "";
            employeeForm["company_email"] = "";
            employeeForm["company_name"] = "";
            employeeForm["company_description"] = "";
            employeeForm["company_address"] = "";
            employeeForm["company_pin_code"] = "";
            employeeForm["company_website"] = "";
            employeeForm["company_contact"] = "";
            employeeForm["company_poc"] = "";
            employeeForm["company_poc_contact"] = "";
            employeeForm["user_password"] = "";

            this.setState({
                employeeForm: employeeForm
            });

            this.addEmployee({ ...this.state.employeeForm });

        }

    };

    //Validation Method
    validate = () => {
        let employeeForm = { ...this.state.employeeForm };


        let errors = {};
        let isValid = true;


        if (employeeForm["user_email"] === "") {
            isValid = false;
            errors["user_email"] = "*Please Enter User Email";


        } else {

            if (typeof employeeForm["user_email"] !== "undefined") {
                //regular expression for email validation
                var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                if (!employeeForm["user_email"].match(pattern)) {
                    isValid = false;
                    errors["user_email"] = "*Please Enter Valid User Email Address";
                }
            }
        }

        if (employeeForm["company_email"] === "") {
            isValid = false;
            errors["company_email"] = "*Please Enter Company Email";


        } else {

            if (typeof employeeForm["company_email"] !== "undefined") {
                //regular expression for email validation
                var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                if (!employeeForm["company_email"].match(pattern)) {
                    isValid = false;
                    errors["company_email"] = "*Please Enter Valid Company Email Address";
                }
            }
        }


        if (employeeForm["company_name"] === "") {
            isValid = false;
            errors["company_name"] = "*Please Enter Company Name";


        }
        if (employeeForm["company_description"] === "") {
            isValid = false;
            errors["company_description"] = "*Please Enter Company Description";


        }
        if (employeeForm["company_address"] === "") {
            isValid = false;
            errors["company_address"] = "*Please Enter Company Address";


        }
        if (employeeForm["company_pin_code"] === "") {
            isValid = false;
            errors["company_pin_code"] = "*Please Enter Company Pin Code";


        }
        if (employeeForm["company_website"] === "") {
            isValid = false;
            errors["company_website"] = "*Please Enter Company Website";


        }
        if (employeeForm["company_contact"] === "") {
            isValid = false;
            errors["company_contact"] = "*Please Enter Company Contact";


        }
        if (employeeForm["company_poc"] === "") {
            isValid = false;
            errors["company_poc"] = "*Please Enter Company Poc";


        }
        if (employeeForm["company_poc_contact"] === "") {
            isValid = false;
            errors["company_poc_contact"] = "*Please Enter Company Poc Contact";


        }


        if (employeeForm["user_password"] === "") {
            isValid = false;
            errors["user_password"] = "*Please Enter User Password";


        } else {
            if (typeof employeeForm["user_password"] !== "undefined") {
                //regular expression for password validation
                var pattern = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)
                if (!employeeForm["user_password"].match(pattern)) {
                    isValid = false;
                    errors["user_password"] = "*Minimum eight characters, one letter and one number";
                }
            }
        }

        this.setState({
            errors: errors
        });

        return isValid;
    };



    renderBreadcrumbs() {
        return (
            <Breadcrumbs>
                <Link to='/'>
                    Home
        </Link>
          Employee
            </Breadcrumbs>
        );
    };

    render() {
        return (
            <Page actionBar={this.renderBreadcrumbs()} title='Employee'>
                <Panel title='Add Employee'>


                    <form onSubmit={this.handleSubmit} method="post">

                        <Input
                            label='User Email Address'
                            placeholder='Enter User Email Address'
                            type="text"
                            name="user_email"
                            onChange={(event) => this.handleChange("user_email", event)}
                            value={this.state.employeeForm.user_email}
                        />
                        <div className="text-danger">{this.state.errors.user_email}</div>
                        <Input
                            label='Company Email Address'
                            placeholder='Enter Company Email Address'
                            type="text"
                            name="company_email"
                            onChange={(event) => this.handleChange("company_email", event)}
                            value={this.state.employeeForm.company_email}
                        />
                        <div className="text-danger">{this.state.errors.company_email}</div>

                        <Input
                            label='Company Name'
                            placeholder='Enter Company Name'
                            type="text"
                            name="company_name"
                            onChange={(event) => this.handleChange("company_name", event)}
                            value={this.state.employeeForm.company_name}
                        />
                        <div className="text-danger">{this.state.errors.company_name}</div>

                        <Input
                            label='Company Description'
                            placeholder='Enter Company Description'
                            type="text"
                            name="company_description"
                            onChange={(event) => this.handleChange("company_description", event)}
                            value={this.state.employeeForm.company_description}
                        />
                        <div className="text-danger">{this.state.errors.company_description}</div>

                        <Input
                            label='Company Address'
                            placeholder='Enter Company Address'
                            type="text"
                            name="company_address"
                            onChange={(event) => this.handleChange("company_address", event)}
                            value={this.state.employeeForm.company_address}
                        />
                        <div className="text-danger">{this.state.errors.company_address}</div>

                        <Input
                            label='Company Pin Code'
                            placeholder='Enter Company Pin Code'
                            type="text"
                            name="company_pin_code"
                            onChange={(event) => this.handleChange("company_pin_code", event)}
                            value={this.state.employeeForm.company_pin_code}
                        />
                        <div className="text-danger">{this.state.errors.company_pin_code}</div>

                        <Input
                            label='Company Website'
                            placeholder='Enter Company Website'
                            type="text"
                            name="company_website"
                            onChange={(event) => this.handleChange("company_website", event)}
                            value={this.state.employeeForm.company_website}
                        />
                        <div className="text-danger">{this.state.errors.company_website}</div>

                        <Input
                            label='Company Contact'
                            placeholder='Enter Company Contact'
                            type="text"
                            name="company_contact"
                            onChange={(event) => this.handleChange("company_contact", event)}
                            value={this.state.employeeForm.company_contact}
                        />
                        <div className="text-danger">{this.state.errors.company_contact}</div>



                        <Input
                            label='Company Poc'
                            placeholder='Enter Company Poc'
                            type="text"
                            name="company_poc"
                            onChange={(event) => this.handleChange("company_poc", event)}
                            value={this.state.employeeForm.company_poc}
                        />
                        <div className="text-danger">{this.state.errors.company_poc}</div>
                        <Input
                            label='Company Poc Contact'
                            placeholder='Enter Company Poc Contact'
                            type="text"
                            name="company_poc_contact"
                            onChange={(event) => this.handleChange("company_poc_contact", event)}
                            value={this.state.employeeForm.company_poc_contact}
                        />
                        <div className="text-danger">{this.state.errors.company_poc_contact}</div>

                        <Input
                            label='User Password'
                            placeholder='Enter User Password'
                            type="password"
                            name="user_password"
                            onChange={(event) => this.handleChange("user_password", event)}
                            value={this.state.employeeForm.user_password}
                        />
                        <div className="text-danger">{this.state.errors.user_password}</div>

                        <div className="add_btn">
                            <Button type="add" title="Add Employee">Add Employee</Button>
                        </div>
                    </form>
                </Panel>

            </Page>
        );
    }
}
