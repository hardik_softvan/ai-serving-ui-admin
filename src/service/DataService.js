import {request} from './UtilService';
import {BASE_URL} from '../constant/APIs';

export async function addEmployeeAPI(data){
   
    return await request({
        url: BASE_URL + "/add_user",
        method: 'POST',
        body: JSON.stringify(data)
    }); 

};

