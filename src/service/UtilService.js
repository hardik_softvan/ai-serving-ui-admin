import Cookies from 'universal-cookie';

 const axios = require('axios');

export const request = (options) => {

    const config = {
        headers: {'Content-Type': 'application/json'},
        url: options['url'],
        method: options['method'],
        data: options['body']
    };


    const cookies = new Cookies();
    if (cookies.get('access_token')) {
        config['headers']['Authorization'] = 'Bearer ' + cookies.get('access_token', {httpOnly: false});
    };

  
    console.log("config",config);

    return axios.request(config)
        .then(response => {
          
            return response.data;
        })
        .catch((error) => {
            return error;
        })
};

